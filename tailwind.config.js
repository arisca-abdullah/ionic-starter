module.exports = {
  purge: {
    enabled: false,
    content: [
      './src/app/*.html',
      './src/app/*.ts',
      './src/app/**/*.html',
      './src/app/**/*.ts',
      './src/app/**/**/*.html',
      './src/app/**/**/*.ts',
      './src/app/**/**/**/*.html',
      './src/app/**/**/**/*.ts'
    ]
  },
  darkMode: 'class', // 'media' | 'class' | false 
  theme: {
    colors: {
      primary: require('tailwindcss/colors').blue,
      secondary: require('tailwindcss/colors').lightBlue,
      tertiary: require('tailwindcss/colors').indigo,
      success: require('tailwindcss/colors').emerald,
      warning: require('tailwindcss/colors').yellow,
      danger: require('tailwindcss/colors').rose,
      light: '#fff',
      medium: require('tailwindcss/colors').gray,
      dark: '#000',
      transparent: 'transparent',
      current: 'currentColor',
    },

    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
