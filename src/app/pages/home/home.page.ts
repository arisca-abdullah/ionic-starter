import { Component } from '@angular/core';
import { UtilsService } from 'src/app/services/utils/utils.service';
import { WebApiService } from 'src/app/services/web-api/web-api.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  constructor(
    private utils: UtilsService,
    private webApi: WebApiService) {}

  ionViewWillEnter() {
    this.webApi.requests({
      requests: [() => this.webApi.getTodo(1)],
      onSuccess: ([todo]) => console.log(todo),
    });
  }

  greet() {
    this.utils.createCustomAlert({
      type: 'info',
      header: 'Hello',
      message: 'Welcome in ionic starter app!',
      buttons: [{ text: 'Close', handler: () => this.utils.dismissPopover() }]
    });
  }
}
