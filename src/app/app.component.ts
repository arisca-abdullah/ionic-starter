import { Component, ViewChild } from '@angular/core';
import { IonRouterOutlet } from '@ionic/angular';
import { UtilsService } from 'src/app/services/utils/utils.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  @ViewChild(IonRouterOutlet) routerOutlet: IonRouterOutlet;

  constructor(private utils: UtilsService) {
    const darkMode = window.matchMedia('(prefers-color-scheme: dark)');
    this.utils.setDarkMode(darkMode.matches);

    darkMode.addEventListener('change', (ev: MediaQueryListEvent) => {
      this.utils.setDarkMode(ev.matches);
    });

    this.utils.overrideBackButton(() => {
      if (this.routerOutlet.canGoBack()) {
        return this.utils.back();
      }

      this.utils.exitApp();
    });
  }
}
