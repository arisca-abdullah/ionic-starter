import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { UtilsService } from 'src/app/services/utils/utils.service';
import { CustomAlertOptions } from 'src/app/components/custom-alert/custom-alert.component';

const requestOptions: any = {
  observe: 'response',
  responseType: 'json'
};

@Injectable({
  providedIn: 'root'
})
export class WebApiService {
  constructor(private httpClient: HttpClient, private utils: UtilsService) { }

  getTodo(id: string|number = '') {
    return this.get('https://jsonplaceholder.typicode.com/todos/' + id);
  }

  async requests(options: RequestSet) {
    try {
      const responses = await Promise.all(options.requests.map(request => request()));
      const responsesBody = responses.map(response => response.body);
      await options?.onSuccess?.(responsesBody);
    } catch (error) {
      if (options?.onError) {
        await options.onError(error);
      } else {
        await this.showError(error);
      }
    } finally {
      await options?.onComplete?.();
    }
  }

  getErrorMessage(error: HttpErrorResponse) {
    if (typeof error?.error === 'string') {
      return error.error;
    }

    if (error?.error?.message) {
      return error.error.message;
    }

    if (error?.error?.error) {
      return error.error.error;
    }

    return error.message;
  }

  showError(error: any, extra: OptionalAlertOpions = {}) {
    const { disableBackdropDismiss, ...extraOptions } = {
      type: 'error',
      header: 'Error',
      message: this.getErrorMessage(error),
      buttons: [{
        text: 'Okay',
        handler: () => this.utils.dismissPopover()
      }],
      ...extra
    };

    return this.utils.createCustomAlert(extraOptions as CustomAlertOptions, disableBackdropDismiss);
  }

  private get(url: string, options?: RequestOptions) {
    return this.httpClient.get(url, { ...requestOptions, ...options }).toPromise();
  }

  private post(url: string, body?: any, options?: RequestOptions) {
    return this.httpClient.post(url, body, { ...requestOptions, ...options }).toPromise();
  }

  private put(url: string, body?: any, options?: RequestOptions) {
    return this.httpClient.put(url, body, { ...requestOptions, ...options }).toPromise();
  }

  private patch(url: string, body?: any, options?: RequestOptions) {
    return this.httpClient.patch(url, body, { ...requestOptions, ...options }).toPromise();
  }

  private delete(url: string, options?: RequestOptions) {
    return this.httpClient.delete(url, { ...requestOptions, ...options }).toPromise();
  }
}

export interface OptionalAlertOpions extends CustomAlertOptions {
  disableBackdropDismiss?: boolean;
}

export type RequestSet = {
  requests: (() => Promise<any>)[];
  onSuccess?: (responses: any[]) => void|any;
  onError?: (error: any) => void|any;
  onComplete?: () => void|any;
};

export type RequestOptions = {
  headers?: HttpHeaders | {[header: string]: string | string[]};
  observe?: 'body' | 'events' | 'response';
  params?: HttpParams | {[param: string]: string | string[]};
  reportProgress?: boolean;
  responseType?: 'arraybuffer' | 'blob' | 'json' | 'text';
  withCredentials?: boolean;
};
