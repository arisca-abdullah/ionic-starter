import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import {
  ActionSheetOptions,
  AlertOptions,
  LoadingOptions,
  PopoverOptions,
  ToastOptions
} from '@ionic/core';

import {
  Platform,
  NavController,
  ActionSheetController,
  AlertController,
  LoadingController,
  PopoverController,
  ToastController
} from '@ionic/angular';

import { CustomAlertComponent, CustomAlertOptions } from 'src/app/components/custom-alert/custom-alert.component';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {
  private temp: any;

  constructor(
    private router: Router,
    private platform: Platform,
    private navCtrl: NavController,
    private actionSheetCtrl: ActionSheetController,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    private popoverCtrl: PopoverController,
    private toastCtrl: ToastController) { }

  get isDarkMode() {
    return document.documentElement.classList.contains('dark');
  }

  get temporaryData() {
    return this.temp;
  }

  set temporaryData(data: any) {
    this.temp = data;
  }

  generateArray(end: number, start?: number, step?: number) {
    return Array.from(this.range(end, start, step));
  }

  parseJson(data: string) {
    try { return JSON.parse(data); }
    catch { return data; }
  }

  setDarkMode(darkMode: boolean) {
    if (darkMode) {
      document.documentElement.classList.add('dark');
    }

    if (!darkMode) {
      document.documentElement.classList.remove('dark');
    }
  }

  toggleDarkMode() {
    this.setDarkMode(!this.isDarkMode);
  }

  parseParams(paramMap: string) {
    paramMap = paramMap.split('%2F').join('/');
    return this.parseJson(paramMap);
  }

  stringifyParams(params: any) {
    params = typeof params === 'string' ? params : JSON.stringify(params);
    return params.split('/').join('%2F');
  }

  navigate(route: string, params?: any) {
    if (params) {
      route += `/${this.stringifyParams(params)}`;
    }

    return this.router.navigate([route]);
  }

  navigateByUrl(route: string, params?: any) {
    if (params) {
      route += `/${this.stringifyParams(params)}`;
    }

    return this.router.navigateByUrl(route);
  }

  navigateRoot(route: string, params?: any) {
    if (params) {
      route += `/${this.stringifyParams(params)}`;
    }

    return this.navCtrl.navigateRoot(route);
  }

  back() {
    return this.navCtrl.pop();
  }

  overrideBackButton(callback: () => any | void) {
    return this.platform.backButton.subscribeWithPriority(10, callback);
  }

  exitApp() {
    // eslint-disable-next-line @typescript-eslint/dot-notation
    navigator['app'].exitApp();
  }

  platformReady() {
    return this.platform.ready();
  }

  async createActionSheet(options: ActionSheetOptions) {
    const actionSheet = await this.actionSheetCtrl.create(options);
    actionSheet.present();

    return actionSheet;
  }

  async createAlert(options: AlertOptions) {
    const alert = await this.alertCtrl.create(options);
    alert.present();

    return alert;
  }

  async createLoader(options: LoadingOptions) {
    const loader = await this.loadingCtrl.create(options);
    loader.present();

    return loader;
  }

  async createPopover(options: PopoverOptions) {
    const popover = await this.popoverCtrl.create(options);
    popover.present();

    return popover;
  }

  async dismissPopover(data?: any) {
    return this.popoverCtrl.dismiss(data);
  }

  async createToast(options: ToastOptions) {
    const toast = await this.toastCtrl.create(options);
    toast.present();

    return toast;
  }

  createCustomAlert(options: CustomAlertOptions, disableBackdropDismiss = false) {
    return this.createPopover({
      component: CustomAlertComponent,
      componentProps: { options },
      cssClass: 'alert-popover center-popover',
      mode: 'ios',
      backdropDismiss: !disableBackdropDismiss
    });
  }

  async getStorage(key: string) {
    await this.platform.ready();
    return this.parseJson(localStorage.getItem(key));
  }

  async setStorage(key: string, value: any) {
    await this.platform.ready();

    localStorage.setItem(key, JSON.stringify(value));
    return value;
  }

  async removeStorage(key: string) {
    await this.platform.ready();

    const value = await this.getStorage(key);
    localStorage.removeItem(key);
    return value;
  }

  async clearStorage() {
    await this.platform.ready();

    localStorage.clear();
    return true;
  }

  private *range(end: number, start: number = 1, step: number = 1) {
    for (let i = start; i <= end; i = i + step) {
      yield i;
    }
  }
}

export { environment } from 'src/environments/environment';
